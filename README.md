## This playbook sets up the file servers.

- Create the root for the mount points
- Installs the required DSC scripts from PS Gallery
- Initializes, formats and labels the new disks
- Mounts them to folders

## Variables

- mount_point: **Root mount folder for the disks**
- disk_ids: **Physical disk id that windows see's**
- disk_list: **Folder name and label that the disks will get**

## Example

 ```
- mount_point: c:\mount
- disk_ids:
   - 1
   - 2
- disk_list:
   - disk1
   - disk2
 ```

 This will create initialize, format and label 2 disks, called disk1 and disk2 respectively. It will also create a mount point at c:\mount and then mount disk1 and disk2 as folders underneath it. 